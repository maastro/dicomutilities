# Dicom Utilities

Dicom Utilities is a Java library providing utility classes for working with medical images in [DICOM](www.dicomstandard.org/) format. It contains adapted code from the [dcm4che](https://www.dcm4che.org/) library for performing common DICOM operations such as C-ECHO, C-FIND, C-MOVE and C-STORE. In addition, a DicomRetriever class is provided, which combines a move SCU and store SCU to enable DICOM moves to oneself (similar to C-GET).

### Dependencies
 * dcm4che-core
 * dcm4che-imagio
 * dcm4che-net

### Timeouts configuration

For the DICOM query and retrieve operations, several timeouts can be configured. These are listed below.

###### Accept timeout
- Timeout in milliseconds for receiving an association acceptance (A-ASSOCIATE-AC) after sending an association request (A-ASSOCIATE-RQ).
- Default value: 0 (no timeout)
- Applies to: FindScu, MoveScu, StoreScu

###### Connect timeout
- Timeout in milliseconds on TCP socket connection attempt.
- Default value: 0 (no timeout)
- Applies to: FindScu, MoveScu, StoreScu

###### Idle timeout
- Timeout in milliseconds for receiving a DIMSE-RQ after opening an association.
- Default value: 0 (no timeout)
- Applies to: FindScu, MoveScu, StoreScu, StoreScp

###### Release timeout
- Timeout in milliseconds for receiving an association release response (A-RELEASE-RP) after sending an association release request (A-RELEASE-RQ).
- Default value: 0 (no timeout)
- Applies to: FindScu, MoveScu, StoreScu, StoreScp

###### Request timeout
- Timeout in milliseconds for receiving an association request (A-ASSOCIATE-RQ).
- Default value: 0 (no timeout)
- Applies to: StoreScp

###### Response timeout
- Timeout in milliseconds for receiving outstanding response messages.
- Default value: 0 (no timeout)
- Applies to: FindScu, StoreScu

###### Retrieve timeout
- Timeout in milliseconds for receiving outstanding C-MOVE responses.
- Default value: 0 (no timeout)
- Applies to: MoveScu

###### Store timeout
- Timeout in milliseconds for completing the C-STORE in DicomRetriever after association release.
- Default value: 30000
- Applies to: DicomRetriever

### License
?
