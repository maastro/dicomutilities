package nl.maastro.dicomUtils.suite;

import org.junit.platform.runner.JUnitPlatform;
import org.junit.platform.suite.api.IncludeTags;
import org.junit.platform.suite.api.SelectPackages;
import org.junit.runner.RunWith;

@RunWith(JUnitPlatform.class)
@SelectPackages("nl.maastro.dicomUtils")
@IncludeTags(TestTags.FUNCTIONAL_TEST) 
public class FunctionalTests {
    
}
