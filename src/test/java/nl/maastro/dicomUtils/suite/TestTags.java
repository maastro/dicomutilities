package nl.maastro.dicomUtils.suite;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Tag;

public class TestTags {
    
    public static final String FUNCTIONAL_TEST = "FunctionalTest";
    
    public static final String INTEGRATION_TEST = "IntegrationTest";
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.TYPE, ElementType.METHOD })
    @Tag(FUNCTIONAL_TEST)
    public @interface FunctionalTest {
    }
    
    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.TYPE, ElementType.METHOD })
    @Tag(INTEGRATION_TEST)
    public @interface IntegrationTest {
    }
    
}
    
