package nl.maastro.dicomUtils.qr;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import nl.maastro.dicomUtils.suite.TestTags.IntegrationTest;

@IntegrationTest
public class EchoScuTest {

    // Put application entity configuration here
    private static final String LOCAL_AETITLE = "SCU";
    private static final String REMOTE_AETITLE = "SCP";
    private static final String REMOTE_HOST = "127.0.0.1";
    private static final int REMOTE_PORT = 104;
    
    @Test
    public void test() throws Exception  {
        boolean success = EchoScu.execute(LOCAL_AETITLE, REMOTE_AETITLE, REMOTE_HOST, REMOTE_PORT);
        assertTrue(success);
    }
}
