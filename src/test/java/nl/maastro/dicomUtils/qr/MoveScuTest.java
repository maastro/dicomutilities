package nl.maastro.dicomUtils.qr;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;

import org.dcm4che3.net.Priority;
import org.dcm4che3.net.Status;
import org.dcm4che3.net.pdu.AAbort;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.maastro.dicomUtils.exception.MoveScuException;
import nl.maastro.dicomUtils.suite.TestTags.IntegrationTest;

@IntegrationTest
public class MoveScuTest {

    // Put application entity configuration here
    private static final String LOCAL_AETITLE = "SCU";
    private static final String REMOTE_AETITLE = "SCP";
    private static final String REMOTE_HOST = "127.0.0.1";
    private static final int REMOTE_PORT = 104;
    private static final String DESTINATION_AETITLE = "DEST";
    
    private static final String INVALID_LOCAL_AETITLE = "UNKNOWN";
    private static final String INVALID_REMOTE_HOST = "UNKNOWN";
    private static final int INVALID_REMOTE_PORT = 105;
    
    // put test DICOM UIDs here
    private static final String STUDY_INSTANCE_UID = null;
    private static final String SERIES_INSTANCE_UID = null;
    private static final String SOP_INSTANCE_UID = null;
    
    private static MoveScu moveScu;

    @BeforeAll
    public static void setup() throws IOException {
        moveScu = new MoveScu(LOCAL_AETITLE, REMOTE_AETITLE, REMOTE_HOST, REMOTE_PORT);
    }
    
    @Test
    public void testMoveStudy() throws Exception  {
        assumeTrue(STUDY_INSTANCE_UID != null);
        moveScu.moveStudy(DESTINATION_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID);
    }

    @Test
    public void testMoveSeries() throws Exception {
        assumeTrue(STUDY_INSTANCE_UID != null);
        assumeTrue(SERIES_INSTANCE_UID != null);
        moveScu.moveSeries(DESTINATION_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID);
    }
    
    @Test
    public void testMoveImage() throws Exception  {
        assumeTrue(STUDY_INSTANCE_UID != null);
        assumeTrue(SERIES_INSTANCE_UID != null);
        assumeTrue(SOP_INSTANCE_UID != null);
        moveScu.moveImage(DESTINATION_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID, SOP_INSTANCE_UID);
    }
    
    @Test
    public void testInvalidLocalAeTitle() throws IOException {
    	MoveScu invalidMoveScu = new MoveScu(INVALID_LOCAL_AETITLE, REMOTE_AETITLE, REMOTE_HOST, REMOTE_PORT);
    	Throwable exception = assertThrows(Exception.class, 
                () -> invalidMoveScu.moveImage(DESTINATION_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID, SOP_INSTANCE_UID));
    	assertTrue(exception.getCause() instanceof AAbort);
    }
    
    @Test
    public void testInvalidRemoteHost() throws Exception {
    	MoveScu invalidMoveScu = new MoveScu(LOCAL_AETITLE, REMOTE_AETITLE, INVALID_REMOTE_HOST, REMOTE_PORT);
    	Throwable exception = assertThrows(Exception.class, 
                () -> invalidMoveScu.moveImage(DESTINATION_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID, SOP_INSTANCE_UID));
    	assertTrue(exception.getCause() instanceof UnknownHostException);
    }
    
    @Test
    public void testInvalidRemotePort() throws Exception {
    	MoveScu invalidMoveScu = new MoveScu(LOCAL_AETITLE, REMOTE_AETITLE, REMOTE_HOST, INVALID_REMOTE_PORT);
    	Throwable exception = assertThrows(Exception.class, 
                () -> invalidMoveScu.moveImage(DESTINATION_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID, SOP_INSTANCE_UID));
    	assertTrue(exception.getCause() instanceof ConnectException);
    }
    
    @Test
    public void testUnknownDestinationAeTitle() {
    	MoveScuException exception = assertThrows(MoveScuException.class, 
                () -> moveScu.moveImage(INVALID_LOCAL_AETITLE, Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID, SOP_INSTANCE_UID));
    	assertEquals(Status.MoveDestinationUnknown, exception.getResponse().getStatusCode());
    }
}