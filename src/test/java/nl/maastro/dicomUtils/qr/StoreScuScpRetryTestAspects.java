package nl.maastro.dicomUtils.qr;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.function.ThrowingConsumer;

public class StoreScuScpRetryTestAspects {
    
    private static final ThrowingConsumer<JoinPoint> DO_NOTHING = jp -> {};
    
    
    @Aspect("perthis(execution(org.dcm4che3.net.AssociationHandler.new(..)))")
    public static class AssociationHandlerAspect {
        
        private ThrowingConsumer<JoinPoint> doBeforeNegotiate = DO_NOTHING;
        
        @Before("execution(org.dcm4che3.net.pdu.AAssociateAC org.dcm4che3.net.AssociationHandler.negotiate("
                + "org.dcm4che3.net.Association, org.dcm4che3.net.pdu.AAssociateRQ))")
        public void beforeNegotiate(JoinPoint joinPoint) throws Throwable {
            doBeforeNegotiate.accept(joinPoint);
        }
        
        public void doBeforeNegotiate(ThrowingConsumer<JoinPoint> doBeforeNegotiate) {
            this.doBeforeNegotiate = doBeforeNegotiate;
        }
        
    }
    
    
    @Aspect("perthis(execution(nl.maastro.dicomUtils.qr.StoreScp.new(..)))")
    public static class StoreScpAspect {
        
        private ThrowingConsumer<JoinPoint> doAfterStore = DO_NOTHING;
        
        private ThrowingConsumer<JoinPoint> doBeforeOnDimseRQ = DO_NOTHING;
        
        @After("execution(void nl.maastro.dicomUtils.qr.StoreScp.store("
                + "org.dcm4che3.net.Association, String, String, String, "
                + "org.dcm4che3.net.PDVInputStream, java.io.File))")
        public void afterStore(JoinPoint joinPoint) throws Throwable {
            doAfterStore.accept(joinPoint);
        }
        
        @Before("execution(void org.dcm4che3.net.service.BasicCStoreSCP.onDimseRQ("
                + "org.dcm4che3.net.Association, org.dcm4che3.net.pdu.PresentationContext,"
                + "org.dcm4che3.net.Dimse, org.dcm4che3.data.Attributes,"
                + "org.dcm4che3.net.PDVInputStream))")
        public void beforeOnDimseRQ(JoinPoint joinPoint) throws Throwable {
            doBeforeOnDimseRQ.accept(joinPoint);
        }

        public void doAfterStore(ThrowingConsumer<JoinPoint> doAfterStore) {
            this.doAfterStore = doAfterStore;
        }

        public void doBeforeOnDimseRQ(ThrowingConsumer<JoinPoint> doBeforeOnDimseRQ) {
            this.doBeforeOnDimseRQ = doBeforeOnDimseRQ;
        }

    }
    
    
    @Aspect("perthis(execution(nl.maastro.dicomUtils.qr.StoreScu.new(..)))")
    public static class StoreScuAspect {
        
        private int numberOfAttempts = 0;
        
        private ThrowingConsumer<JoinPoint> doBeforeStoreFile = DO_NOTHING;

        @Before("execution(java.util.List<nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse> "
                + "nl.maastro.dicomUtils.qr.StoreScu.store(java.util.HashSet<java.io.File>, int))")
        public void beforeNewStoreAttempt(JoinPoint joinPoint) {
            numberOfAttempts++;
        }
        
        @Before("execution(nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse "
                + "nl.maastro.dicomUtils.qr.StoreScu.store(org.dcm4che3.net.Association, java.io.File, int))")
        public void beforeStoreFile(JoinPoint joinPoint) throws Throwable {
            doBeforeStoreFile.accept(joinPoint);
        }
        
        public int getNumberOfAttempts() {
            return numberOfAttempts;
        }
        
        public void setNumberOfAttempts(int numberOfAttempts) {
            this.numberOfAttempts = numberOfAttempts;
        }

        public void doBeforeStoreFile(ThrowingConsumer<JoinPoint> doBeforeStoreFile) {
            this.doBeforeStoreFile = doBeforeStoreFile;
        }

    }

}
