package nl.maastro.dicomUtils.qr;

import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.dcm4che3.net.Priority;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.maastro.dicomUtils.suite.TestTags.IntegrationTest;

@IntegrationTest
public class DicomRetrieverTest {
    
    private static final String LOCAL_AETITLE = "TEST";
    private static final int LOCAL_PORT = 105;
    
    private static final String REMOTE_AETITLE = "SCP";
    private static final String REMOTE_HOST = "127.0.0.1";
    private static final int REMOTE_PORT = 104;
    
    // put test DICOM UIDs here
    private static final String STUDY_INSTANCE_UID = null;
    private static final String SERIES_INSTANCE_UID = null;
    private static final String SOP_INSTANCE_UID = null;
    
    private static Path tempDirectory;
    private static DicomRetriever dicomRetriever;
    
    @BeforeAll
    public static void setup() throws IOException, GeneralSecurityException {
        tempDirectory = Paths.get(UUID.randomUUID().toString());
        dicomRetriever = new DicomRetriever(LOCAL_AETITLE, LOCAL_PORT, REMOTE_AETITLE, 
                REMOTE_HOST, REMOTE_PORT, tempDirectory);
    }
    
    @AfterAll
    public static void teardown() throws IOException {
        FileUtils.forceDelete(tempDirectory.toFile());
    }
    
    @Test
    public void testRetrieveStudy() throws Exception  {
        assumeTrue(STUDY_INSTANCE_UID != null);
        dicomRetriever.retrieveStudy(Priority.NORMAL, STUDY_INSTANCE_UID);
    }
    
    @Test
    public void testRetrieveSeries() throws Exception {
        assumeTrue(STUDY_INSTANCE_UID != null);
        assumeTrue(SERIES_INSTANCE_UID != null);
        dicomRetriever.retrieveSeries(Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID);
    }
    
    @Test
    public void testRetrieveImage() throws Exception  {
        assumeTrue(STUDY_INSTANCE_UID != null);
        assumeTrue(SERIES_INSTANCE_UID != null);
        assumeTrue(SOP_INSTANCE_UID != null);
        dicomRetriever.retrieveImage(Priority.NORMAL, STUDY_INSTANCE_UID, SERIES_INSTANCE_UID, SOP_INSTANCE_UID);
    }
    
}
