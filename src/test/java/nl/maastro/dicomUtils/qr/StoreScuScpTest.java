package nl.maastro.dicomUtils.qr;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.dcm4che3.data.UID;
import org.dcm4che3.net.TransferCapability;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import nl.maastro.dicomUtils.common.DimseStatusClass;
import nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse;
import nl.maastro.dicomUtils.suite.TestTags.FunctionalTest;

@FunctionalTest
public class StoreScuScpTest {
    
    private static final String SCU_AETITLE = "SCU";
    private static final String SCP_AETITLE = "SCP";
    private static final String SCP_HOST = "127.0.0.1";
    private static final int SCP_PORT = 111;
    private static final String DICOM_FILES_DIRECTORY = "src\\test\\resources\\data\\12345";
    private static final String RTPLAN_FILE = "src\\test\\resources\\data\\RTPLAN\\FO-3630512758406762316.dcm";

    private StoreScu storeScu;
    private StoreScp storeScp;
    private Path tempDirectory;
    private List<File> dicomFiles;
    
    @BeforeEach
    public void setupForEach() throws GeneralSecurityException, IOException {
    	dicomFiles = getDicomFiles(Paths.get(DICOM_FILES_DIRECTORY));
        storeScu = new StoreScu(SCU_AETITLE, SCP_AETITLE, SCP_HOST, SCP_PORT);
        tempDirectory = Paths.get(UUID.randomUUID().toString());
        
        // Only accept CT, RTSTRUCT, RTPLAN and RTDOSE
        List<TransferCapability> transferCapabilities = Arrays.asList(
        		new TransferCapability(null, UID.CTImageStorage, TransferCapability.Role.SCP, "*"),
        		new TransferCapability(null, UID.RTStructureSetStorage, TransferCapability.Role.SCP, "*"),
				new TransferCapability(null, UID.RTDoseStorage, TransferCapability.Role.SCP, "*"));
        storeScp = new StoreScp(SCP_AETITLE, SCP_PORT, tempDirectory, transferCapabilities);
        storeScp.start();
    }
    
    @AfterEach
    public void teardownForEach() throws IOException {
        storeScp.stop();
        if (tempDirectory.toFile().exists()) {
            FileUtils.forceDelete(tempDirectory.toFile());
        }
    }
    
    @Test
    public void storeTest() throws InterruptedException, ExecutionException {
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        List<StoreScuResponse> responses = future.get();
        int numberOfStoredFiles = tempDirectory.toFile().listFiles().length;
        assertTrue(responses.parallelStream()
                .allMatch(response -> response.hasStatus(DimseStatusClass.SUCCESS)));
        assertEquals(dicomFiles.size(), numberOfStoredFiles);
    }
    
    @Test
    public void invalidAeTitleTest() throws IOException {
        String invalidRemoteAeTitle = "INVALID_AETITLE";
        StoreScu invalidStoreScu = new StoreScu(SCU_AETITLE, invalidRemoteAeTitle, SCP_HOST, SCP_PORT);
        Throwable exception = assertThrows(ExecutionException.class, 
                () -> invalidStoreScu.store(dicomFiles).get());
        assertTrue(exception.getCause().getCause().getMessage().contains("reason: 7 - called-AE-title-not-recognized"));
    }
    
    @Test
    public void invalidPortTest() throws IOException {
        int invalidPort = SCP_PORT + 1000;
        StoreScu invalidStoreScu = new StoreScu(SCU_AETITLE, SCP_AETITLE, SCP_HOST, invalidPort);
        Throwable exception = assertThrows(ExecutionException.class, 
                () -> invalidStoreScu.store(dicomFiles).get());
        assertTrue(exception.getCause().getCause().getMessage().contains("Connection refused"));
    }
    
    @Test
    public void unsupportedSopClassTest() {
    	dicomFiles.add(new File(RTPLAN_FILE));
    	assertThrows(ExecutionException.class, () -> storeScu.store(dicomFiles).get());
	  	int numberOfStoredFiles = tempDirectory.toFile().listFiles().length;
	  	assertEquals(dicomFiles.size()-1, numberOfStoredFiles);
    }
    
    private static List<File> getDicomFiles(Path directory) throws IOException {
        return Files.walk(directory)
                .filter(path -> isDicomFile(path))
                .map(path -> path.toFile())
                .collect(Collectors.toList());
    }
    
    private static boolean isDicomFile(Path filePath) {
        return Files.isRegularFile(filePath) 
        		&& filePath.getFileName().toString().endsWith(".dcm");
    }

}

