package nl.maastro.dicomUtils.qr;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import nl.maastro.dicomUtils.suite.TestTags.IntegrationTest;

@IntegrationTest
public class StoreScuTest {

	private static StoreScu storeScu;
	
	// Put application entity configuration here
	private static final String LOCAL_AETITLE = "SCU";
    private static final String REMOTE_AETITLE = "SCP";
    private static final String REMOTE_HOST = "127.0.0.1";
    private static final int REMOTE_PORT = 104;

    private static final String DICOM_FILES_DIRECTORY = "src\\test\\resources\\data\\12345";
    
    @BeforeAll
    public static void setup() throws GeneralSecurityException, IOException {
        storeScu = new StoreScu(LOCAL_AETITLE, REMOTE_AETITLE, REMOTE_HOST, REMOTE_PORT);
    }

    @Test
    public void testStore() throws Exception {
        List<File> dicomFiles = Files.walk(Paths.get(DICOM_FILES_DIRECTORY))
                .filter(Files::isRegularFile)
                .map(path -> path.toFile())
                .collect(Collectors.toList());
        storeScu.store(dicomFiles);
    }
    
}

