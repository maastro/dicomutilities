package nl.maastro.dicomUtils.qr;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Sequence;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.VR;
import org.dcm4che3.net.Priority;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.maastro.dicomUtils.qr.FindScuResponseHandler.FindScuResponse;
import nl.maastro.dicomUtils.suite.TestTags.IntegrationTest;

@IntegrationTest
public class FindScuTest {
    
    private static final Logger logger = LoggerFactory.getLogger(FindScuTest.class);
    private static FindScu findScu;
    
    // Put application entity configuration here
    private static final String LOCAL_AETITLE = "SCU";
    private static final String REMOTE_AETITLE = "SCP";
    private static final String REMOTE_HOST = "localhost";
    private static final int REMOTE_PORT = 4242;
    
    // Specify UIDs here
    private static final String PATIENT_ID = "12345";
    private static final String STUDY_UID = "9999.176707096301547682584419484115766666734";
    private static final String CT_SERIES_UID = "9999.222499342266219596535467323678278275423";
    private static final String RTSTRUCT_SERIES_UID = "9999.262803179494451408271220056323772759131";

    @BeforeAll
    public static void setup() throws IOException {
        findScu = new FindScu(LOCAL_AETITLE, REMOTE_AETITLE, REMOTE_HOST, REMOTE_PORT);
    }

    @Test
    public void testGetStudies() throws Exception {
        Attributes queryKeys = new Attributes();
        queryKeys.setValue(Tag.QueryRetrieveLevel, VR.CS, "STUDY");
        queryKeys.setValue(Tag.PatientID, VR.LO, PATIENT_ID);
        queryKeys.setValue(Tag.StudyInstanceUID, VR.UI, "");
        queryKeys.setValue(Tag.StudyDescription, VR.LO, "");

        List<FindScuResponse> responses = findScu.find(queryKeys, Priority.NORMAL);
        for (FindScuResponse response : responses) {
            if (response != null) {
                String studyUid = response.getDataSetAttribute(Tag.StudyInstanceUID, String.class);
                logger.info("StudyInstanceUID: " + studyUid);
                String studyDescription = response.getDataSetAttribute(Tag.StudyDescription, String.class);
                logger.info("StudyDescription: " + studyDescription);
            }
        }
    }
    
    @Test
    public void testGetRtStructSeries() throws Exception {
        Attributes queryKeys = new Attributes();
        queryKeys.setValue(Tag.QueryRetrieveLevel, VR.CS, "SERIES");
        queryKeys.setValue(Tag.PatientID, VR.LO, PATIENT_ID);
        queryKeys.setValue(Tag.StudyInstanceUID, VR.UI, STUDY_UID);
        queryKeys.setValue(Tag.Modality, VR.CS, "RTSTRUCT");
        queryKeys.setValue(Tag.SeriesInstanceUID, VR.UI, "");
        queryKeys.setValue(Tag.SeriesDescription, VR.LO, "");

        List<FindScuResponse> responses = findScu.find(queryKeys, Priority.NORMAL);
        for (FindScuResponse response : responses) {
            if (response != null) {
                String seriesUid = response.getDataSetAttribute(Tag.SeriesInstanceUID, String.class);
                logger.info("SeriesInstanceUID: " + seriesUid);
            }
        }
    }
    
    @Test
    public void testGetRtStructMetadata() throws Exception {
        Attributes queryKeys = new Attributes();
        queryKeys.setValue(Tag.QueryRetrieveLevel, VR.CS, "IMAGE");
        queryKeys.setValue(Tag.PatientID, VR.LO, PATIENT_ID);
        queryKeys.setValue(Tag.StudyInstanceUID, VR.UI, STUDY_UID);
        queryKeys.setValue(Tag.Modality, VR.CS, "RTSTRUCT");
        queryKeys.setValue(Tag.SeriesInstanceUID, VR.UI, RTSTRUCT_SERIES_UID);
        queryKeys.setValue(Tag.SOPInstanceUID, VR.UI, "");
        queryKeys.setValue(Tag.InstanceCreationDate, VR.DA, "");
        queryKeys.newSequence(Tag.ReferencedFrameOfReferenceSequence, 1).add(new Attributes());
        queryKeys.getSequence(Tag.ReferencedFrameOfReferenceSequence).get(0)
            .newSequence(Tag.RTReferencedStudySequence, 1).add(new Attributes());
        queryKeys.getSequence(Tag.ReferencedFrameOfReferenceSequence).get(0)
            .getSequence(Tag.RTReferencedStudySequence).get(0)
            .newSequence(Tag.RTReferencedSeriesSequence, 1).add(new Attributes());
        queryKeys.getSequence(Tag.ReferencedFrameOfReferenceSequence).get(0)
            .getSequence(Tag.RTReferencedStudySequence).get(0)
            .getSequence(Tag.RTReferencedSeriesSequence).get(0)
            .setValue(Tag.SeriesInstanceUID, VR.UI, "");
        
        List<FindScuResponse> responses = findScu.find(queryKeys, Priority.NORMAL);
        
        logger.info("Number of responses:" + responses.size());
        for (FindScuResponse response : responses) {
            if (response != null) {
                Attributes responseData = response.getDataSet();
                if (responseData != null && !responseData.isEmpty()) {
                    String sopUid = responseData.getString(Tag.SOPInstanceUID);
                    logger.info("SOPInstanceUID: " + sopUid);
                    
                    Date instanceCreationDate = responseData.getDate(Tag.InstanceCreationDate);
                    logger.info("InstanceCreationDate: " + instanceCreationDate);
                    
                    Sequence referencedFrameOfReferenceSequence = responseData.getSequence(Tag.ReferencedFrameOfReferenceSequence);
                    Sequence rtReferencedStudySequence = referencedFrameOfReferenceSequence.get(0).getSequence(Tag.RTReferencedStudySequence);
                    Sequence rtReferencedSeriesSequence = rtReferencedStudySequence.get(0).getSequence(Tag.RTReferencedSeriesSequence);
                    String ctSeriesUid = rtReferencedSeriesSequence.get(0).getString(Tag.SeriesInstanceUID);
                    logger.info("ctSeriesUid: " + ctSeriesUid);
                }
            }
        }
    }
    
    @Test
    public void testGetCtSeriesMetaData() throws Exception {
        Attributes queryKeys = new Attributes();
        queryKeys.setValue(Tag.QueryRetrieveLevel, VR.CS, "IMAGE");
        queryKeys.setValue(Tag.PatientID, VR.LO, PATIENT_ID);
        queryKeys.setValue(Tag.StudyInstanceUID, VR.UI, STUDY_UID);
        queryKeys.setValue(Tag.SeriesInstanceUID, VR.UI, CT_SERIES_UID);
        queryKeys.setValue(Tag.Modality, VR.CS, "CT");
        queryKeys.setValue(Tag.AcquisitionDate, VR.DA, "");
        queryKeys.setValue(Tag.SeriesDate, VR.DA, "");
        queryKeys.setValue(Tag.SeriesDescription, VR.LO, "");
        queryKeys.setValue(Tag.Manufacturer, VR.LO, "");
        queryKeys.setValue(Tag.ManufacturerModelName, VR.LO, "");
        
        List<FindScuResponse> responses = findScu.find(queryKeys, Priority.NORMAL);
        
        Date acquisitionDate = responses.get(0).getDataSetAttribute(Tag.AcquisitionDate, Date.class);
        logger.info("AcquisitionDate: " + acquisitionDate);
        
        Date seriesDate = responses.get(0).getDataSetAttribute(Tag.SeriesDate, Date.class);
        logger.info("SeriesDate: " + seriesDate);
        
        String seriesDescription = responses.get(0).getDataSetAttribute(Tag.SeriesDescription, String.class);
        logger.info("SeriesDescription: " + seriesDescription);
        
        String manufacturer = responses.get(0).getDataSetAttribute(Tag.Manufacturer, String.class);
        logger.info("Manufacturer: " + manufacturer);
        
        String manufacturerModelName = responses.get(0).getDataSetAttribute(Tag.ManufacturerModelName, String.class);
        logger.info("ManufacturerModelName: " + manufacturerModelName);
    }
}
