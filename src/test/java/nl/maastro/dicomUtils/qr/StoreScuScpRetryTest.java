package nl.maastro.dicomUtils.qr;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import org.apache.commons.io.FileUtils;
import org.aspectj.lang.Aspects;
import org.dcm4che3.net.Status;
import org.dcm4che3.net.pdu.AAssociateRJ;
import org.dcm4che3.net.service.DicomServiceException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.retry.backoff.FixedBackOffPolicy;

import nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse;
import nl.maastro.dicomUtils.qr.StoreScuScpRetryTestAspects.AssociationHandlerAspect;
import nl.maastro.dicomUtils.qr.StoreScuScpRetryTestAspects.StoreScpAspect;
import nl.maastro.dicomUtils.qr.StoreScuScpRetryTestAspects.StoreScuAspect;
import nl.maastro.dicomUtils.suite.TestTags.FunctionalTest;

@FunctionalTest
public class StoreScuScpRetryTest {
    
    private static final String SCU_AETITLE = "SCU";
    private static final String SCP_AETITLE = "SCP";
    private static final String SCP_HOST = "127.0.0.1";
    private static final int SCP_PORT = 111;
    private static final String DICOM_FILES_DIRECTORY = "src\\test\\resources\\data\\12345";
    private static final int MAX_RETRY_ATTEMPTS = 3;
    private static final int TIMEOUT = 1000;
    private static final int DELAY = 2000;
    
    private Path tempDirectory;
    private List<File> dicomFiles;
    private StoreScu storeScu;
    private StoreScp storeScp;
    
    @BeforeEach
    public void setupForEach() throws GeneralSecurityException, IOException {
        dicomFiles = getDicomFiles(Paths.get(DICOM_FILES_DIRECTORY));
        FixedBackOffPolicy backOffPolicy = new FixedBackOffPolicy();
        storeScu = new StoreScu(SCU_AETITLE, SCP_AETITLE, SCP_HOST, SCP_PORT, MAX_RETRY_ATTEMPTS, backOffPolicy);
        tempDirectory = Paths.get(UUID.randomUUID().toString());
        storeScp = new StoreScp(SCP_AETITLE, SCP_PORT, tempDirectory);
        storeScp.start();
    }
    
    @AfterEach
    public void teardownForEach() throws IOException {
        storeScp.stop();
        if (tempDirectory.toFile().exists()) {
            FileUtils.forceDelete(tempDirectory.toFile());
        }
    }
    
    @Test
    public void shouldRetryOnAcceptTimeoutTest() throws Exception {
        storeScu.setAcceptTimeout(TIMEOUT);
        AssociationHandlerAspect associationHandlerAspect = Aspects.aspectOf(AssociationHandlerAspect.class, 
                storeScp.device.getAssociationHandler());
        associationHandlerAspect.doBeforeNegotiate(joinPoint -> Thread.sleep(DELAY));
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(MAX_RETRY_ATTEMPTS);
    }
    
    @Test
    public void shouldRetryOnConnectTimeoutTest() {
        storeScu.setConnectTimeout(TIMEOUT);
        storeScp.stop();
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(MAX_RETRY_ATTEMPTS);
    }
    
    @Test
    public void shouldRetryOnIdleTimeoutTest() {
        storeScu.setIdleTimeout(TIMEOUT);
        StoreScuAspect storeScuAspect = Aspects.aspectOf(StoreScuAspect.class, storeScu);
        storeScuAspect.doBeforeStoreFile(joinPoint -> Thread.sleep(DELAY));
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(MAX_RETRY_ATTEMPTS);
    }
    
    @Test
    public void shouldRetryOnResponseTimeoutTest() throws InterruptedException {
        storeScu.setResponseTimeout(TIMEOUT);
        StoreScpAspect storeScpAspect = Aspects.aspectOf(StoreScpAspect.class, storeScp);
        storeScpAspect.doAfterStore(joinPoint -> Thread.sleep(DELAY));
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(MAX_RETRY_ATTEMPTS);
    }
    
    @Test
    public void shouldRetryOnTransientAssociateReject() {
        AssociationHandlerAspect associationHandlerAspect = Aspects.aspectOf(AssociationHandlerAspect.class, 
                storeScp.device.getAssociationHandler());
        associationHandlerAspect.doBeforeNegotiate(joinPoint -> { 
            throw new AAssociateRJ(
                    AAssociateRJ.RESULT_REJECTED_TRANSIENT,
                    AAssociateRJ.SOURCE_SERVICE_PROVIDER_PRES,
                    AAssociateRJ.REASON_NO_REASON_GIVEN);
        });
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(MAX_RETRY_ATTEMPTS);
    }
    
    @Test
    public void shouldNotRetryOnPermanentAssociateReject() {
        AssociationHandlerAspect associationHandlerAspect = Aspects.aspectOf(AssociationHandlerAspect.class, 
                storeScp.device.getAssociationHandler());
        associationHandlerAspect.doBeforeNegotiate(joinPoint -> { 
            throw new AAssociateRJ(
                    AAssociateRJ.RESULT_REJECTED_PERMANENT,
                    AAssociateRJ.SOURCE_SERVICE_PROVIDER_PRES,
                    AAssociateRJ.REASON_NO_REASON_GIVEN);
        });
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(1);
    }
    
    @Test
    public void shouldRetryOnDuplicateInvocationTest() {
        StoreScpAspect storeScpAspect = Aspects.aspectOf(StoreScpAspect.class, storeScp);
        storeScpAspect.doBeforeOnDimseRQ(joinPoint -> {
            throw new DicomServiceException(Status.DuplicateInvocation);
        });
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(MAX_RETRY_ATTEMPTS);
    }
    
    @Test
    public void shouldNotRetryOnSopClassNotSupportedTest() {
        StoreScpAspect storeScpAspect = Aspects.aspectOf(StoreScpAspect.class, storeScp);
        storeScpAspect.doBeforeOnDimseRQ(joinPoint -> {
            throw new DicomServiceException(Status.SOPclassNotSupported);
        });
        CompletableFuture<List<StoreScuResponse>> future = storeScu.store(dicomFiles);
        assertThrows(ExecutionException.class, () -> future.get());
        assertNumberOfAttemptsEquals(1);
    }
    
    private static List<File> getDicomFiles(Path directory) throws IOException {
        return Files.walk(directory)
                .filter(path -> isDicomFile(path))
                .map(path -> path.toFile())
                .collect(Collectors.toList());
    }
    
    private static boolean isDicomFile(Path filePath) {
        return Files.isRegularFile(filePath) 
                && filePath.getFileName().toString().endsWith(".dcm");
    }
    
    private void assertNumberOfAttemptsEquals(int expected) {
        StoreScuAspect storeScuAspect = Aspects.aspectOf(StoreScuAspect.class, storeScu);
        assertEquals(expected, storeScuAspect.getNumberOfAttempts());
    }
 
}
