package nl.maastro.dicomUtils.exception;

import java.util.List;

import nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse;

public class StoreScuException extends Exception {

	private static final long serialVersionUID = 6662414179434815800L;
	
	private List<StoreScuResponse> responses;
	
	public StoreScuException(String message) {
		super(message);
	}
	
	public StoreScuException(String message, List<StoreScuResponse> responses) {
		super(message);
		this.responses = responses;
	}
	
	public List<StoreScuResponse> getResponses() {
		return this.responses;
	}
	
}
