package nl.maastro.dicomUtils.exception;

import nl.maastro.dicomUtils.qr.MoveScuResponseHandler.MoveScuResponse;

public class MoveScuException extends Exception {
	
	private static final long serialVersionUID = 5689370009602452423L;
	
	private MoveScuResponse response;
	
	public MoveScuException(String message) {
		super(message);
	}
	
	public MoveScuException(String message, MoveScuResponse response) {
		super(message);
		this.response = response;
	}

	public MoveScuResponse getResponse() {
		return response;
	}

}
