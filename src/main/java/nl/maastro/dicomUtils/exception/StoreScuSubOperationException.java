package nl.maastro.dicomUtils.exception;

import nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse;

public class StoreScuSubOperationException extends Exception {

    private static final long serialVersionUID = 2437464706976615139L;
    
    private StoreScuResponse response;
    
    public StoreScuSubOperationException(String message) {
        super(message);
    }
    
    public StoreScuSubOperationException(String message, StoreScuResponse response) {
        super(message);
        this.response = response;
    }
    
    public StoreScuResponse getResponse() {
        return this.response;
    }
    
}
