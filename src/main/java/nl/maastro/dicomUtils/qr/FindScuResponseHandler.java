package nl.maastro.dicomUtils.qr;

import java.util.ArrayList;
import java.util.List;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.DimseRSPHandler;

import nl.maastro.dicomUtils.common.DimseResponse;

public class FindScuResponseHandler extends DimseRSPHandler {
    
    private List<FindScuResponse> responses;

    public FindScuResponseHandler(int id) {
        super(id);
        this.responses = new ArrayList<>();
    }

    @Override
    public void onDimseRSP(Association as, Attributes cmd, Attributes data) {
        super.onDimseRSP(as, cmd, data);
        responses.add(new FindScuResponse(cmd, data));
    }

    public List<FindScuResponse> getResponses() {
        return responses;
    }
    
    public static class FindScuResponse extends DimseResponse {
        public FindScuResponse(Attributes commandSet, Attributes dataSet) {
            super(commandSet, dataSet);
        }
    }
    
}
