package nl.maastro.dicomUtils.qr;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.dcm4che3.net.Association;
import org.dcm4che3.net.PDVInputStream;

import nl.maastro.dicomUtils.qr.MoveScuResponseHandler.MoveScuResponse;

/**
 * DicomRetriever is a combination of a MOVE-SCU and STORE-SCP that enables 
 * moving DICOM files to oneself (similar to a C-GET operation). 
 */
public class DicomRetriever {
    
    private final String localAeTitle;
    private int storeTimeOut = 30000;
    private MoveScu moveScu;
    private DicomRetrieverStoreScp storeScp;
    
    public DicomRetriever(String localAeTitle, int localPort, String remoteAeTitle, 
            String remoteHost, int remotePort, Path storageDirectory) 
            throws IOException, GeneralSecurityException {
        this.localAeTitle = localAeTitle;
        this.moveScu = new MoveScu(localAeTitle, remoteAeTitle, remoteHost, remotePort);
        this.storeScp = new DicomRetrieverStoreScp(localAeTitle, localPort, storageDirectory);
    }
    
    public List<File> retrievePatient(int priority, String patientId) throws Exception {
        try{
            this.storeScp.start();
            MoveScuResponse response = this.moveScu.movePatient(this.localAeTitle, priority, patientId);
            int numberOfImagesMoved = response.getNumberOfCompletedSuboperations();
            waitForOutstandingStoreRequests(numberOfImagesMoved);
            return storeScp.getStoredFiles();
        } finally {
            this.storeScp.stop();
        }
    }
    
    public List<File> retrieveStudy(int priority, String studyInstanceUid) throws Exception {
        try{
            this.storeScp.start();
            MoveScuResponse response = this.moveScu.moveStudy(this.localAeTitle, priority, studyInstanceUid);
            int numberOfImagesMoved = response.getNumberOfCompletedSuboperations();
            waitForOutstandingStoreRequests(numberOfImagesMoved);
            return storeScp.getStoredFiles();
        } finally {
            this.storeScp.stop();
        }
    }
    
    public List<File> retrieveSeries(int priority, String studyInstanceUid, String seriesInstanceUid) throws Exception {
        try{
            this.storeScp.start();
            MoveScuResponse response = this.moveScu.moveSeries(this.localAeTitle, priority, studyInstanceUid, seriesInstanceUid);
            int numberOfImagesMoved = response.getNumberOfCompletedSuboperations();
            waitForOutstandingStoreRequests(numberOfImagesMoved);
            return storeScp.getStoredFiles();
        } finally {
            this.storeScp.stop();
        }
    }
    
    public File retrieveImage(int priority, String studyInstanceUid, String seriesInstanceUid, 
            String sopInstanceUid) throws Exception {
        try{
            this.storeScp.start();
            MoveScuResponse response = this.moveScu.moveImage(this.localAeTitle, priority, studyInstanceUid, 
                    seriesInstanceUid, sopInstanceUid);
            int numberOfImagesMoved = response.getNumberOfCompletedSuboperations();
            waitForOutstandingStoreRequests(numberOfImagesMoved);
            List<File> storedFiles = storeScp.getStoredFiles();
            if (storedFiles.isEmpty()) {
                return null;
            } else {
                return storedFiles.get(0);
            }
        } finally {
            this.storeScp.stop();
        }
    }
    
    public void setMoveScuAcceptTimeout(int acceptTimeout) {
        this.moveScu.setAcceptTimeout(acceptTimeout);
    }
    
    public void setMoveScuConnectTimeout(int connectTimeout) {
        this.moveScu.setConnectTimeout(connectTimeout);
    }
    
    public void setMoveScuIdleTimeout(int idleTimeout) {
        this.moveScu.setIdleTimeout(idleTimeout);
    }
    
    public void setMoveScuReleaseTimeout(int releaseTimeout) {
        this.moveScu.setReleaseTimeout(releaseTimeout);
    }
    
    public void setMoveScuRetrieveTimeout(int retrieveTimeout, boolean isTotal) {
        this.moveScu.setRetrieveTimeout(retrieveTimeout, isTotal);
    }
    
    public void setStoreScpIdleTimeout(int idleTimeout) {
        this.storeScp.setIdleTimeout(idleTimeout);
    }
    
    public void setStoreScpReleaseTimeout(int releaseTimeout) {
        this.storeScp.setReleaseTimeout(releaseTimeout);
    }
    
    public void setStoreScpRequestTimeout(int requestTimeout) {
        this.storeScp.setRequestTimeout(requestTimeout);
    }
    
    public void setStoreTimeOut(int storeTimeOut) {
        this.storeTimeOut = storeTimeOut;
    }

    private void waitForOutstandingStoreRequests(int numberOfFilesSent) 
            throws InterruptedException, TimeoutException {
        final int POLLING_INTERVAL = 1000;
        long startTime = System.currentTimeMillis();
        int numberOfFilesReceived;
        while ((numberOfFilesReceived = storeScp.getStoredFiles().size()) != numberOfFilesSent) {
            if (System.currentTimeMillis() - startTime < storeTimeOut) {
                Thread.sleep(POLLING_INTERVAL);                
            } else {
                throw new TimeoutException("Outstanding C-STORE requests still not processed after " 
                        + storeTimeOut + " milliseconds; "
                        + "numberOfFilesSent=" + numberOfFilesSent + ", "
                        + "numberOfFilesReceived=" + numberOfFilesReceived);
            }
        }
    }
    
    
    private static class DicomRetrieverStoreScp extends StoreScp {
        
        private List<File> storedFiles = new ArrayList<>();
        
        public DicomRetrieverStoreScp(String aeTitle, int port, Path storageDirectory)
                throws IOException, GeneralSecurityException {
            super(aeTitle, port, storageDirectory);
        }
        
        @Override
        protected File store(Association association, String sopInstanceUid, String sopClassUid,
                String transferSyntax, PDVInputStream data) throws IOException {
            File file = super.store(association, sopInstanceUid, sopClassUid, transferSyntax, data);
            storedFiles.add(file);
            return file;
        }
        
        public List<File> getStoredFiles() {
            return storedFiles;
        }
        
    }
}
