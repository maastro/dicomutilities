package nl.maastro.dicomUtils.qr;

import java.util.ArrayList;
import java.util.List;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.DimseRSPHandler;
import org.dcm4che3.net.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.maastro.dicomUtils.common.DimseResponse;

public class MoveScuResponseHandler extends DimseRSPHandler {
    
    private static final Logger logger = LoggerFactory.getLogger(MoveScuResponseHandler.class);
    
    private List<MoveScuResponse> responses = new ArrayList<>();
    
    public MoveScuResponseHandler(int msgId) {
        super(msgId);
    }

    @Override
    public void onDimseRSP(Association as, Attributes cmd, Attributes data) {
        super.onDimseRSP(as, cmd, data);
        MoveScuResponse response = new MoveScuResponse(cmd, null);
        logger.debug("C-MOVE response: " + response);
        this.responses.add(response);
    }
    
    public List<MoveScuResponse> getResponses() {
        return this.responses;
    }
    
    public MoveScuResponse getLastResponse() {
        int numberOfResponses = this.responses.size();
        if (numberOfResponses > 0) {
            return this.responses.get(numberOfResponses - 1);
        } else {
            return null;
        } 
    }
    
    public static class MoveScuResponse extends DimseResponse {
    	
    	/*
        *  Descriptions of C-MOVE statuses in accordance with: 
        * - http://dicom.nema.org/medical/dicom/current/output/chtml/part04/sect_C.4.2.html
        * - http://dicom.nema.org/medical/dicom/current/output/chtml/part07/chapter_9.html#sect_9.1.4.1.7
        */
    	private static final String STATUS_ONE_OR_MORE_FAILURES = "Sub-operations Complete - One or more Failures";
    	private static final String STATUS_UNABLE_TO_CALCULATE_NUMBER_OF_MATCHES = "Out of Resources - Unable to calculate number of matches";
    	private static final String STATUS_UNABLE_TO_PERFORM_SUB_OPERATIONS = "Out of Resources - Unable to perform sub-operations";
    	private static final String STATUS_MOVE_DESTINATION_UNKNOWN = "Move Destination unknown";
    	private static final String STATUS_DATASET_DOES_NOT_MATCH_SOP_CLASS = "Data set does not match SOP Class";
    	private static final String STATUS_UNABLE_TO_PROCESS = "Unable to Process";
    	
        public MoveScuResponse(Attributes commandSet, Attributes dataSet) {
            super(commandSet, dataSet);
        }
        
        public static String getStatusDescription(Integer statusCode) {
        	if (statusCode == null) {
        		return null;
        	} else if (statusCode == Status.OneOrMoreFailures) {
        		return STATUS_ONE_OR_MORE_FAILURES;
        	} else if (statusCode == Status.UnableToCalculateNumberOfMatches) {
        		return STATUS_UNABLE_TO_CALCULATE_NUMBER_OF_MATCHES;
        	} else if (statusCode == Status.UnableToPerformSubOperations) {
        		return STATUS_UNABLE_TO_PERFORM_SUB_OPERATIONS;
        	} else if (statusCode == Status.MoveDestinationUnknown) {
        		return STATUS_MOVE_DESTINATION_UNKNOWN;
        	} else if (statusCode == Status.DataSetDoesNotMatchSOPClassError) {
        		return STATUS_DATASET_DOES_NOT_MATCH_SOP_CLASS;
        	} else if (statusCode >= 0xC000 && statusCode <= 0xCFFF) {
        		return STATUS_UNABLE_TO_PROCESS;
        	} else {
        		return DimseResponse.getStatusDescription(statusCode);
        	}
        }
        
        public String getErrorComment() {
            return getCommandSetAttribute(Tag.ErrorComment, String.class);
        }
        
        public Integer getNumberOfCompletedSuboperations() {
            return getCommandSetAttribute(Tag.NumberOfCompletedSuboperations, Integer.class);
        }
        
        public Integer getNumberOfFailedSuboperations() {
            return getCommandSetAttribute(Tag.NumberOfFailedSuboperations, Integer.class);
        }
        
        public Integer getNumberOfRemainingSuboperations() {
            return getCommandSetAttribute(Tag.NumberOfRemainingSuboperations, Integer.class);
        }
        
        public Integer getNumberOfWarningSuboperations() {
            return getCommandSetAttribute(Tag.NumberOfWarningSuboperations, Integer.class);
        }
        
        @Override
        public String toString() {
            return "MoveScuResponse: [\n"
            		+ "messageIdBeingRespondedTo=" + getMessageIdBeingRespondedTo() + ",\n"
                    + "statusCode=" + getStatusCode() + ",\n"
                    + "errorComment=" + getErrorComment() + ",\n"
                    + "completedSuboperations=" + getNumberOfCompletedSuboperations() + ",\n"
                    + "remainingSuboperations=" + getNumberOfRemainingSuboperations() + ",\n"
                    + "failedSuboperations=" + getNumberOfFailedSuboperations() + ",\n"
                    + "warningSubOperations=" + getNumberOfWarningSuboperations() + "]";
        }
    }
}
