/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2017
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s): -
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package nl.maastro.dicomUtils.qr;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.ElementDictionary;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.data.VR;
import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Connection;
import org.dcm4che3.net.Device;
import org.dcm4che3.net.DimseRSPHandler;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.maastro.dicomUtils.common.DimseStatusClass;
import nl.maastro.dicomUtils.common.Monitor;
import nl.maastro.dicomUtils.exception.MoveScuException;
import nl.maastro.dicomUtils.qr.MoveScuResponseHandler.MoveScuResponse;

public class MoveScu {
    
    private static final Logger logger = LoggerFactory.getLogger(MoveScu.class);
    
    private static final int UNLIMITED_ASYNC_OPS = 0;
    private static final String INFORMATION_MODEL_UID = UID.StudyRootQueryRetrieveInformationModelMove;
    private static final String[] IVR_LE_FIRST = {
            UID.ImplicitVRLittleEndian,
            UID.ExplicitVRLittleEndian,
            UID.ExplicitVRBigEndian
        };
    
    private static AtomicInteger nextMessageId = new AtomicInteger(1);
    
    private Device device;
    private final ApplicationEntity applicationEntity;
    private final Connection localConnection;
    private final Connection remoteConnection;
    private final AAssociateRQ request;
    
    
    public MoveScu(String localAeTitle, String remoteAeTitle, String remoteHost, int remotePort) throws IOException {
        this.device = new Device("movescu");
        this.device.setConnectionMonitor(new Monitor());
        
        this.applicationEntity = new ApplicationEntity(localAeTitle);
        this.device.addApplicationEntity(this.applicationEntity);
        
        this.localConnection = new Connection();
        this.localConnection.setMaxOpsInvoked(UNLIMITED_ASYNC_OPS);
        this.localConnection.setMaxOpsPerformed(UNLIMITED_ASYNC_OPS);
        this.device.addConnection(this.localConnection);
        this.applicationEntity.addConnection(this.localConnection);
        
        this.remoteConnection = new Connection();
        this.remoteConnection.setHostname(remoteHost);
        this.remoteConnection.setPort(remotePort);
        this.remoteConnection.setTlsProtocols(this.localConnection.getTlsProtocols());
        this.remoteConnection.setTlsCipherSuites(this.localConnection.getTlsCipherSuites());

        this.request = new AAssociateRQ();
        this.request.setCalledAET(remoteAeTitle);
        this.request.addPresentationContext(new PresentationContext(1, INFORMATION_MODEL_UID, IVR_LE_FIRST));
    }
    
    public MoveScuResponse move(String destinationAeTitle, Attributes keys, int priority) throws Exception {
    	MoveScuResponseHandler responseHandler = new MoveScuResponseHandler(getNextMessageId());
    	move(destinationAeTitle, keys, priority, responseHandler);
    	MoveScuResponse response = responseHandler.getLastResponse();
    	assertMoveIsSuccessful(response);
    	return response;
    }
    
    public void move(String destinationAeTitle, Attributes keys, int priority, DimseRSPHandler responseHandler) throws Exception {
    	ExecutorService executorService = Executors.newSingleThreadExecutor();
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        device.setExecutor(executorService);
        device.setScheduledExecutor(scheduledExecutorService);
        try {
            Association association = this.applicationEntity.connect(this.localConnection, this.remoteConnection, this.request);
            try {
                association.cmove(INFORMATION_MODEL_UID, priority, keys, null, destinationAeTitle, responseHandler);
                association.waitForOutstandingRSP();
                IOException exception = association.getException();
                if (exception != null) {
                	throw exception;
                }
            } finally {
                releaseAssociation(association);
            }
        } catch (Exception e) {
            throw new Exception("C-MOVE has failed", e);
        } finally {
            executorService.shutdown();
            scheduledExecutorService.shutdown();
        }
    }
    
    public void assertMoveIsSuccessful(MoveScuResponse response) throws Exception {
    	if (response == null) {
    		throw new MoveScuException("C-MOVE has completed without any DIMSE responses");
    	} else {
    		Integer statusCode = response.getStatusCode();
    		if (statusCode == null) {
    			throw new MoveScuException("Unable to determine C-MOVE status; no status code in DIMSE response: " + response, response);
    		}
    		
    		DimseStatusClass statusClass = DimseStatusClass.fromStatusCode(statusCode);
    		String statusDescription = MoveScuResponse.getStatusDescription(statusCode);
    		String statusMessage = statusClass + " (" + Integer.toHexString(statusCode) + "): " + statusDescription; 
    		switch (statusClass) {
	    		case SUCCESS:
	    			logger.info("C-MOVE has completed successfully; status=" + statusMessage);
	    			return;
	    		case WARNING:
	    			logger.warn("C-MOVE has completed with warnings; status=" + statusMessage);
	    			return;
	    		case CANCEL:
	    			throw new MoveScuException("C-MOVE has been terminated with CANCEL indication; status=" + statusMessage, response);
	    		case FAILURE:
	    			throw new MoveScuException("C-MOVE has failed; status=" + statusMessage, response);
	    		case PENDING:
	    			throw new MoveScuException("C-MOVE is still pending; status=" + statusMessage, response);
	    		default:
	    			throw new MoveScuException("Unknown C-MOVE status; status=" + statusMessage, response);
    		}
    	}
    }
    
    private void releaseAssociation(Association association) {
        if (association.isReadyForDataTransfer()) {
            try {
                association.release();
            } catch (IOException e) {
                logger.error("Failed to release association", e);
            }
        }
    }

    public MoveScuResponse movePatient(String thirdPartyAet, int priority, String patientId) throws Exception {
        Attributes keys = new Attributes();
        VR vr = ElementDictionary.vrOf(Tag.PatientID, keys.getPrivateCreator(Tag.PatientID));
        keys.setString(Tag.PatientID, vr, patientId);
        keys.setString(Tag.QueryRetrieveLevel, VR.CS, "PATIENT");
        return this.move(thirdPartyAet, keys, priority);
    }

    public MoveScuResponse moveStudy(String thirdPartyAet, int priority, String studyInstanceUid) throws Exception {
        Attributes keys = new Attributes();
        VR vr = ElementDictionary.vrOf(Tag.StudyInstanceUID, keys.getPrivateCreator(Tag.StudyInstanceUID));
        keys.setString(Tag.StudyInstanceUID, vr, studyInstanceUid);
        keys.setString(Tag.QueryRetrieveLevel, VR.CS, "STUDY");
        return this.move(thirdPartyAet, keys, priority);
    }

    public MoveScuResponse moveSeries(String thirdPartyAet, int priority,String studyInstanceUid, 
            String seriesInstanceUid) throws Exception {
        Attributes keys = new Attributes();
        VR vr = ElementDictionary.vrOf(Tag.StudyInstanceUID, keys.getPrivateCreator(Tag.StudyInstanceUID));
        keys.setString(Tag.StudyInstanceUID, vr, studyInstanceUid);
        vr = ElementDictionary.vrOf(Tag.SeriesInstanceUID, keys.getPrivateCreator(Tag.SeriesInstanceUID));
        keys.setString(Tag.SeriesInstanceUID, vr, seriesInstanceUid);
        keys.setString(Tag.QueryRetrieveLevel, VR.CS, "SERIES");
        return this.move(thirdPartyAet, keys, priority);
    }

    public MoveScuResponse moveImage(String thirdPartyAet, int priority, String studyInstanceUid, 
            String seriesInstanceUid, String sopInstanceUid) throws Exception {
        Attributes keys = new Attributes();
        VR vr = ElementDictionary.vrOf(Tag.StudyInstanceUID, keys.getPrivateCreator(Tag.StudyInstanceUID));
        keys.setString(Tag.StudyInstanceUID, vr, studyInstanceUid);
        vr = ElementDictionary.vrOf(Tag.SeriesInstanceUID, keys.getPrivateCreator(Tag.SeriesInstanceUID));
        keys.setString(Tag.SeriesInstanceUID, vr, seriesInstanceUid);
        vr = ElementDictionary.vrOf(Tag.SOPInstanceUID, keys.getPrivateCreator(Tag.SOPInstanceUID));
        keys.setString(Tag.SOPInstanceUID, vr, sopInstanceUid);
        keys.setString(Tag.QueryRetrieveLevel, VR.CS, "IMAGE");
        return this.move(thirdPartyAet, keys, priority);
    }
    
    private int getNextMessageId() {
    	return nextMessageId.getAndAdd(1);
    }
    
    public void setAcceptTimeout(int acceptTimeout) {
        this.localConnection.setAcceptTimeout(acceptTimeout);
    }
    
    public void setConnectTimeout(int connectTimeout) {
        this.localConnection.setConnectTimeout(connectTimeout);
    }
    
    public void setIdleTimeout(int idleTimeout) {
        this.localConnection.setIdleTimeout(idleTimeout);
    }
    
    public void setReleaseTimeout(int releaseTimeout) {
        this.localConnection.setReleaseTimeout(releaseTimeout);
    }

    public void setRetrieveTimeout(int retrieveTimeout, boolean isTotal) {
        this.localConnection.setRetrieveTimeout(retrieveTimeout);
        this.localConnection.setRetrieveTimeoutTotal(isTotal);
    }
    
}
