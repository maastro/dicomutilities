/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2017
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s): -
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package nl.maastro.dicomUtils.qr;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.data.VR;
import org.dcm4che3.io.DicomOutputStream;
import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Connection;
import org.dcm4che3.net.Device;
import org.dcm4che3.net.PDVInputStream;
import org.dcm4che3.net.Status;
import org.dcm4che3.net.TransferCapability;
import org.dcm4che3.net.pdu.PresentationContext;
import org.dcm4che3.net.service.BasicCEchoSCP;
import org.dcm4che3.net.service.BasicCStoreSCP;
import org.dcm4che3.net.service.DicomServiceRegistry;
import org.dcm4che3.util.SafeClose;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.maastro.dicomUtils.common.Monitor;

public class StoreScp extends BasicCStoreSCP {
    
    private static final Logger logger = LoggerFactory.getLogger(StoreScp.class);

    private static final int UNLIMITED_ASYNC_OPS = 0;
    private static final String DCM_EXTENSION = ".dcm";
    
    protected Device device;
    protected ApplicationEntity applicationEntity;
    protected Connection localConnection;
    
	private Path storageDirectory;
    
    /** Creates a DICOM C-STORE service class provider.
     * 
     * @param aeTitle application entity title
     * @param port port number
     * @param storageDirectory directory where files should be stored
     * @throws GeneralSecurityException 
     * @throws IOException 
     */
    public StoreScp(String aeTitle, int port, Path storageDirectory) 
            throws IOException, GeneralSecurityException {
    	// Accept all SOP classes and transfer syntaxes
    	this(aeTitle, port, storageDirectory, Collections.singletonList(
    			new TransferCapability(null, "*", TransferCapability.Role.SCP, "*")));
    }
    
    /** Creates a DICOM C-STORE service class provider.
     * 
     * @param aeTitle application entity title
     * @param port port number
     * @param storageDirectory directory where files should be stored
     * @param transferCapabilities list of supported SOP classes and transfer syntaxes
     * @throws GeneralSecurityException 
     * @throws IOException 
     */
    public StoreScp(String aeTitle, int port, Path storageDirectory, 
    		List<TransferCapability> transferCapabilities) 
            throws IOException, GeneralSecurityException {
        this.device = new Device("storescp");
        this.device.setConnectionMonitor(new Monitor());
        this.device.setDimseRQHandler(createServiceRegistry());
       
        this.applicationEntity = new ApplicationEntity(aeTitle);
        transferCapabilities.forEach(tc -> this.applicationEntity.addTransferCapability(tc));
        this.device.addApplicationEntity(this.applicationEntity);
       
        this.localConnection = new Connection();
        this.localConnection.setPort(port);        
        this.localConnection.setMaxOpsInvoked(UNLIMITED_ASYNC_OPS);
        this.localConnection.setMaxOpsPerformed(UNLIMITED_ASYNC_OPS);
        this.device.addConnection(this.localConnection);
        this.applicationEntity.addConnection(this.localConnection);
        Files.createDirectories(storageDirectory);
        this.storageDirectory = storageDirectory;
    }
    
    @Override
    protected void store(Association as, PresentationContext pc,
            Attributes rq, PDVInputStream data, Attributes rsp)
                    throws IOException {
        rsp.setInt(Tag.Status, VR.US, Status.Pending);
        try {
            String sopClassUid = rq.getString(Tag.AffectedSOPClassUID);
            String sopInstanceUid = rq.getString(Tag.AffectedSOPInstanceUID);
            String transferSyntax = pc.getTransferSyntax();
            store(as, sopInstanceUid, sopClassUid, transferSyntax, data);
            rsp.setInt(Tag.Status, VR.US, Status.Success);
        } catch (Exception e) {
            rsp.setInt(Tag.Status, VR.US, Status.ProcessingFailure);
        }
    }
    
    protected File store(Association association, String sopInstanceUid, String sopClassUid, 
            String transferSyntax, PDVInputStream data) throws IOException {
        File file = new File(this.storageDirectory.toFile(), sopInstanceUid + DCM_EXTENSION);
        store(association, sopInstanceUid, sopClassUid, transferSyntax, data, file);
        return file;
    }
    
    protected void store(Association association, String sopInstanceUid, String sopClassUid, 
            String transferSyntax, PDVInputStream data, File file) throws IOException {
        try (FileOutputStream outputStream = new FileOutputStream(file);
                FileChannel channel = outputStream.getChannel()) {
            channel.lock();
            store(association, sopInstanceUid, sopClassUid, transferSyntax, data, outputStream);
            logger.info("Stored new file: " + file.getName());
        } catch (Exception e) {
            logger.error("Error while storing file: " + file, e);
            throw e;
        }
    }
    
    private void store(Association association, String sopInstanceUid, String sopClassUid, 
            String transferSyntax, PDVInputStream data, OutputStream outputStream) throws IOException {
        DicomOutputStream dicomOutputStream = new DicomOutputStream(new BufferedOutputStream(outputStream), 
                UID.ExplicitVRLittleEndian);
        Attributes fileMetaInformation = association.createFileMetaInformation(
                sopInstanceUid, sopClassUid, transferSyntax);
        try {
            dicomOutputStream.writeFileMetaInformation(fileMetaInformation);
            data.copyTo(dicomOutputStream);
        } finally {
            SafeClose.close(dicomOutputStream);
        }
    }
    
    public void start() throws IOException, GeneralSecurityException {
        this.device.setExecutor(Executors.newCachedThreadPool());
        this.device.setScheduledExecutor(Executors.newSingleThreadScheduledExecutor());
        this.device.bindConnections();
        logger.info("Started DICOM C-STORE SCP on port " + this.localConnection.getPort());
    }
    
    public void stop() {
        this.device.unbindConnections();
        ScheduledExecutorService scheduledExecutor = this.device.getScheduledExecutor();
        scheduledExecutor.shutdown();
        ExecutorService executor = (ExecutorService) this.device.getExecutor();
        executor.shutdown();
        logger.info("Stopped DICOM C-STORE SCP on port " + this.localConnection.getPort());
    }
    
    private DicomServiceRegistry createServiceRegistry() {
        DicomServiceRegistry serviceRegistry = new DicomServiceRegistry();
        serviceRegistry.addDicomService(new BasicCEchoSCP());
        serviceRegistry.addDicomService(this);
        return serviceRegistry;
    }
    
	public Path getStorageDirectory() {
        return storageDirectory;
    }

    public void setIdleTimeout(int idleTimeout) {
        this.localConnection.setIdleTimeout(idleTimeout);
    }
	
	public void setReleaseTimeout(int releaseTimeout) {
        this.localConnection.setReleaseTimeout(releaseTimeout);
    }
    
    public void setRequestTimeout(int requestTimeout) {
        this.localConnection.setRequestTimeout(requestTimeout);
    }
	
}