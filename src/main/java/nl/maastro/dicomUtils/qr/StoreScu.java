package nl.maastro.dicomUtils.qr;

import java.io.File;
import java.io.IOException;
import java.net.ConnectException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.stream.Collectors;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.data.UID;
import org.dcm4che3.imageio.codec.Decompressor;
import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Connection;
import org.dcm4che3.net.DataWriter;
import org.dcm4che3.net.DataWriterAdapter;
import org.dcm4che3.net.Device;
import org.dcm4che3.net.DimseRSPHandler;
import org.dcm4che3.net.Priority;
import org.dcm4che3.net.Status;
import org.dcm4che3.net.Timeout;
import org.dcm4che3.net.pdu.AAbort;
import org.dcm4che3.net.pdu.AAssociateRJ;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.retry.RetryContext;
import org.springframework.retry.RetryPolicy;
import org.springframework.retry.backoff.BackOffPolicy;
import org.springframework.retry.backoff.NoBackOffPolicy;
import org.springframework.retry.policy.AlwaysRetryPolicy;
import org.springframework.retry.policy.NeverRetryPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

import nl.maastro.dicomUtils.common.DicomObject;
import nl.maastro.dicomUtils.common.DimseStatusClass;
import nl.maastro.dicomUtils.common.Monitor;
import nl.maastro.dicomUtils.exception.StoreScuException;
import nl.maastro.dicomUtils.exception.StoreScuSubOperationException;
import nl.maastro.dicomUtils.qr.StoreScuResponseHandler.StoreScuResponse;

public class StoreScu {

    private static final Logger logger = LoggerFactory.getLogger(StoreScu.class);

    private static final int UNLIMITED_ASYNC_OPS = 0;

    private Device device;
    private final ApplicationEntity applicationEntity;
    private final Connection localConnection;
    private final Connection remoteConnection;
    private final String remoteAeTitle;
    private final RetryTemplate retryTemplate;
 
    public StoreScu(String localAeTitle, String remoteAeTitle, String remoteHost, int remotePort) throws IOException {
        this(localAeTitle, remoteAeTitle, remoteHost, remotePort, new NeverRetryPolicy(), new NoBackOffPolicy());
    }

    public StoreScu(String localAeTitle, String remoteAeTitle, String remoteHost, int remotePort, int maxRetryAttempts,
            BackOffPolicy backOffPolicy) throws IOException {
        this(localAeTitle, remoteAeTitle, remoteHost, remotePort, new StoreScuSimpleRetryPolicy(maxRetryAttempts), backOffPolicy);
    }
    
    public StoreScu(String localAeTitle, String remoteAeTitle, String remoteHost, int remotePort,
            RetryPolicy retryPolicy, BackOffPolicy backOffPolicy) throws IOException {

        this.device = new Device("storescu");
        this.device.setConnectionMonitor(new Monitor());

        this.applicationEntity = new ApplicationEntity(localAeTitle);
        this.device.addApplicationEntity(this.applicationEntity);

        this.localConnection = new Connection();
        this.localConnection.setMaxOpsInvoked(UNLIMITED_ASYNC_OPS);
        this.localConnection.setMaxOpsPerformed(UNLIMITED_ASYNC_OPS);
        this.device.addConnection(this.localConnection);
        this.applicationEntity.addConnection(this.localConnection);

        this.remoteConnection = new Connection();
        this.remoteConnection.setHostname(remoteHost);
        this.remoteConnection.setPort(remotePort);
        this.remoteConnection.setTlsProtocols(this.localConnection.getTlsProtocols());
        this.remoteConnection.setTlsCipherSuites(this.localConnection.getTlsCipherSuites());
        this.remoteAeTitle = remoteAeTitle;

        this.retryTemplate = RetryTemplate.builder()
                .customPolicy(retryPolicy)
                .customBackoff(backOffPolicy)
                .build();
    }

    public CompletableFuture<List<StoreScuResponse>> store(Collection<File> files) {
        return this.store(files, Priority.NORMAL);
    }

    public CompletableFuture<List<StoreScuResponse>> store(Collection<File> files, int priority) {
        return CompletableFuture.supplyAsync(() -> {
            try {
                return storeWithRetry(files, priority);
            } catch (Exception e) {
                throw new CompletionException(e);
            }
        });
    }

    private List<StoreScuResponse> storeWithRetry(Collection<File> files, int priority) throws Exception {
        HashSet<File> fileSet = new HashSet<>(files);
        return retryTemplate.execute(context -> {
            return store(fileSet, priority);
        });
    }

    private List<StoreScuResponse> store(HashSet<File> files, int priority) throws Exception {
        logger.info("Starting DICOM C-STORE for " + files.size() + " files");

        ExecutorService executorService = Executors.newSingleThreadExecutor();
        ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
        this.device.setExecutor(executorService);
        this.device.setScheduledExecutor(scheduledExecutorService);

        try {
            AAssociateRQ request = createAssociationRequest(files);
            Association association = this.applicationEntity.connect(this.localConnection, this.remoteConnection, request);

            List<StoreScuResponse> responses = new ArrayList<>();
            for (File file : new HashSet<File>(files)) {
                try {
                    StoreScuResponse response = store(association, file, priority);
                    responses.add(response);
                    files.remove(file);
                } catch (AAbort e) {
                    logger.error("C-STORE is aborted", e);
                    throw e;
                } catch (StoreScuSubOperationException e) {
                    logger.error("C-STORE suboperation failed", e);
                    responses.add(e.getResponse());
                } catch (Exception e) {
                    logger.error("C-STORE suboperation failed", e);
                    responses.add(null);
                }
            }
            
            releaseAssociation(association);
            assertStoreIsSuccessful(responses);
            return responses;
            
        } catch (Exception e) {
            throw new Exception("C-STORE has failed", e);
        } finally {
            executorService.shutdown();
            scheduledExecutorService.shutdown();
        }
    }

    private StoreScuResponse store(Association association, File file, int priority) throws Exception {
        StoreScuResponseHandler responseHandler = new StoreScuResponseHandler(association.nextMessageID());
        store(association, file, priority, responseHandler);
        StoreScuResponse response = responseHandler.getResponse();
        assertStoreSubOperationIsSuccessful(response);
        return response;
    }

    private void store(Association association, File file, int priority, DimseRSPHandler responseHandler)
            throws IOException, InterruptedException {
        DicomObject dicomObject = new DicomObject(file);
        Attributes fmi = dicomObject.getFileMetaInformation();
        String sopClassUid = fmi.getString(Tag.MediaStorageSOPClassUID);
        String sopInstanceUid = fmi.getString(Tag.MediaStorageSOPInstanceUID);
        String fileTransferSyntax = fmi.getString(Tag.TransferSyntaxUID);
        String transferSyntax = selectTransferSyntax(association, sopClassUid, fileTransferSyntax);

        Attributes data = dicomObject.getDataSet();
        if (!transferSyntax.equals(fileTransferSyntax)) {
            Decompressor.decompress(data, fileTransferSyntax);
        }
        DataWriter dataWriter = new DataWriterAdapter(dicomObject.getDataSet());

        association.cstore(sopClassUid, sopInstanceUid, priority, dataWriter, transferSyntax, responseHandler);
        association.waitForOutstandingRSP();

        IOException exception = association.getException();
        if (exception != null) {
            throw exception;
        }
    }

    private void assertStoreIsSuccessful(List<StoreScuResponse> responses) throws StoreScuException {
        int numberOfSuccesses = filterResponsesOnStatus(responses, DimseStatusClass.SUCCESS).size();
        int numberOfFailures = filterResponsesOnStatus(responses, DimseStatusClass.FAILURE).size();
        int numberOfWarnings = filterResponsesOnStatus(responses, DimseStatusClass.FAILURE).size();
        int numberOfStatusUnknown = filterResponsesOnStatus(responses, DimseStatusClass.UNKNOWN).size();
        int numberOfStatusNull = filterResponsesOnStatus(responses, null).size();
        long numberOfNullResponses = responses.parallelStream()
                .filter(response -> response == null)
                .count();
        
        logger.info("Number of responses with status SUCCESS: " + numberOfSuccesses);
        logger.info("Number of responses with status WARNING: " + numberOfWarnings);
        logger.info("Number of responses with status FAILURE: " + numberOfFailures);
        if (numberOfStatusUnknown > 0)
            logger.info("Number of responses with status UNKNOWN: " + numberOfStatusUnknown);
        if (numberOfStatusNull > 0)
            logger.info("Number of responses with status NULL: " + numberOfStatusNull);
        if (numberOfNullResponses > 0)
            logger.info("Number of NULL responses: " + numberOfNullResponses);
        
        if (numberOfFailures > 0 || numberOfNullResponses > 0 || numberOfStatusNull > 0 || numberOfStatusUnknown > 0) {
            throw new StoreScuException("C-STORE has failed (see log for further information)", responses);
        } else if (numberOfWarnings > 0) {
            logger.warn("C-STORE has completed with warnings (see log for further information)");
        } else {
            logger.info("C-STORE has completed successfully");
        }
    }
    
    private void assertStoreSubOperationIsSuccessful(StoreScuResponse response) throws Exception {
        if (response == null) {
            throw new Exception("Unable to determine status of C-STORE suboperation, because response is null");
        }
        Integer messageId = response.getMessageIdBeingRespondedTo();
        Integer statusCode = response.getStatusCode();
        DimseStatusClass statusClass = DimseStatusClass.fromStatusCode(statusCode);
        switch (statusClass) {
            case SUCCESS:
                logger.trace("C-STORE suboperation has completed successfully; messageId=" + messageId);
                return;
            case WARNING:
                logger.warn("C-STORE suboperation has completed with warnings; messageId=" + messageId);
                return;
            case FAILURE:  
                throw new StoreScuSubOperationException("C-STORE suboperation has failed with statusCode=" + statusCode 
                        + "; messageId=" + messageId, response);
            default:
                throw new StoreScuSubOperationException("Invalid / unknown status for C-STORE suboperation"
                        + "; statusCode=" + statusCode + ", messageId=" + messageId);
        }
    }

    public List<StoreScuResponse> filterResponsesOnStatus(List<StoreScuResponse> responses, DimseStatusClass status) {
        return responses.parallelStream()
                .filter(response -> response != null && response.hasStatus(status))
                .collect(Collectors.toList());
    }

    private AAssociateRQ createAssociationRequest(Collection<File> files) throws Exception {
    	AAssociateRQ request = new AAssociateRQ();
    	request.setCalledAET(remoteAeTitle);
    	request.addPresentationContext(new PresentationContext(1,
                UID.Verification, UID.ImplicitVRLittleEndian));
    	for (File file : files) {
    		try {
    			addPresentationContextsForFile(request, file);
    		} catch (IOException e) {
    			throw new Exception("Failed to set PresentationContext for file: " + file);
    		}
    	}
    	return request;
    }

    private AAssociateRQ addPresentationContextsForFile(AAssociateRQ request, File file) throws IOException {
        DicomObject dicomObject = new DicomObject(file);
        Attributes fmi = dicomObject.getFileMetaInformation();
        String sopClassUid = fmi.getString(Tag.MediaStorageSOPClassUID);
        String transferSyntax = fmi.getString(Tag.TransferSyntaxUID);
        request = addPresentationContextIfNotExists(request, sopClassUid, UID.ExplicitVRLittleEndian);
        request = addPresentationContextIfNotExists(request, sopClassUid, UID.ImplicitVRLittleEndian);
        request = addPresentationContextIfNotExists(request, sopClassUid, transferSyntax);
        return request;
    }

    private AAssociateRQ addPresentationContextIfNotExists(AAssociateRQ request, String sopClassUid,
            String transferSyntax) {
        if (!request.containsPresentationContextFor(sopClassUid, transferSyntax)) {
            int id = request.getNumberOfPresentationContexts() * 2 + 1;
            request.addPresentationContext(new PresentationContext(id, sopClassUid, transferSyntax));
        }
        return request;
    }

    private String selectTransferSyntax(Association association, String sopClassUid, String fileTransferSyntax) {
        Set<String> supportedSyntaxes = association.getTransferSyntaxesFor(sopClassUid);
        if (supportedSyntaxes.contains(fileTransferSyntax)) {
            return fileTransferSyntax;
        } else if (supportedSyntaxes.contains(UID.ExplicitVRLittleEndian)) {
            return UID.ExplicitVRLittleEndian;
        } else {
            return UID.ImplicitVRLittleEndian;
        }
    }

    private void releaseAssociation(Association association) {
        try {
            if (association.isReadyForDataTransfer()) {
                association.release();
            }
            association.waitForSocketClose();
        } catch (IOException | InterruptedException e) {
            logger.error("Failed to release association", e);
        }
    }

    public void setAcceptTimeout(int acceptTimeout) {
        this.localConnection.setAcceptTimeout(acceptTimeout);
    }

    public void setConnectTimeout(int connectTimeout) {
        this.localConnection.setConnectTimeout(connectTimeout);
    }

    public void setIdleTimeout(int idleTimeout) {
        this.localConnection.setIdleTimeout(idleTimeout);
    }

    public void setReleaseTimeout(int releaseTimeout) {
        this.localConnection.setReleaseTimeout(releaseTimeout);
    }

    public void setResponseTimeout(int responseTimeout) {
        this.localConnection.setResponseTimeout(responseTimeout);
    }
    
    public static boolean canRetry(RetryContext context) {
        Throwable lastError = context.getLastThrowable();
        if (lastError == null) {
            return true;
        }
        
        Throwable lastErrorCause = lastError.getCause();
        if (lastErrorCause instanceof ConnectException 
                || lastErrorCause instanceof UnknownHostException
                || lastErrorCause instanceof SocketTimeoutException) {
            // Retry on network connection errors
            return true;
            
        } else if (lastErrorCause instanceof AAssociateRJ) {
            // Retry on transient association rejections
            AAssociateRJ associationReject = (AAssociateRJ) lastErrorCause;
            if (associationReject.getResult() == AAssociateRJ.RESULT_REJECTED_TRANSIENT) {
                return true;
            } else {
                return false;
            }
            
        } else if (lastErrorCause instanceof AAbort) {
            // Retry on timeout errors
            AAbort associationAbort = (AAbort) lastErrorCause;
            if (isCausedByTimeout(associationAbort)) {
                return true;
            } else {
                return false;
            }
            
        } else if (lastErrorCause instanceof StoreScuException) {
            
            StoreScuException storeScuException = (StoreScuException) lastErrorCause;
            List<StoreScuResponse> responses = storeScuException.getResponses();
            for (StoreScuResponse response : responses) {
                if (response != null && response.getStatusCode() == Status.DuplicateInvocation) {
                    // Retry on duplicate invocations (CSTORE is blocked by another operation at the 
                    // remote AE, which is using the same message ID)
                    return true;
                } else if (response != null && response.getStatusCode() == Status.OutOfResources) {
                    // Retry when remote AE is (temporarily) out of resources
                    return true;
                }
            }
            return false;
        
        } else {
            return false;
        }
    }
    
    private static boolean isCausedByTimeout(AAbort associationAbort) {
        for (StackTraceElement stackTraceElement : associationAbort.getStackTrace()) {
            if (stackTraceElement.getClassName() == Timeout.class.getCanonicalName()) {
                return true;
            }
        }
        return false;
    }
    
    
    public static class StoreScuAlwaysRetryPolicy extends AlwaysRetryPolicy {

        private static final long serialVersionUID = 1L;

        @Override
        public boolean canRetry(RetryContext context) {
            if (super.canRetry(context)) {
                return StoreScu.canRetry(context);
            } else {
                return false;
            }
        }

    }
    
    
    public static class StoreScuSimpleRetryPolicy extends SimpleRetryPolicy {

        private static final long serialVersionUID = 1L;

        public StoreScuSimpleRetryPolicy(int maxAttempts) {
            super(maxAttempts);
        }

        @Override
        public boolean canRetry(RetryContext context) {
            if (super.canRetry(context)) {
                return StoreScu.canRetry(context);
            } else {
                return false;
            }
        }

    }

}
