package nl.maastro.dicomUtils.qr;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.DimseRSPHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.maastro.dicomUtils.common.DimseResponse;
import nl.maastro.dicomUtils.common.DimseStatusClass;

public class StoreScuResponseHandler extends DimseRSPHandler {
	
	private static final Logger logger = LoggerFactory.getLogger(StoreScuResponseHandler.class);
	
	private StoreScuResponse response;
	
	public StoreScuResponseHandler(int msgId) {
		super(msgId);
	}
	
	@Override
    public void onDimseRSP(Association as, Attributes cmd, Attributes data) {
        super.onDimseRSP(as, cmd, data);
        this.response = new StoreScuResponse(cmd, null);
        if (this.response.hasStatus(DimseStatusClass.FAILURE)) {
            logger.error("C-STORE response has status FAILURE: " + response);
        } else if (this.response.hasStatus(DimseStatusClass.WARNING)) {
            logger.warn("C-STORE response has status WARNING: " + response);
        } else if (response.hasStatus(DimseStatusClass.UNKNOWN)) {
            logger.warn("C-STORE response has UNKOWN status: " + response);
        } else {
            logger.debug("C-STORE response: " + response);
        }
    }
	
	public StoreScuResponse getResponse() {
		return response;
	}
	
	
	public static class StoreScuResponse extends DimseResponse {
    	
    	public StoreScuResponse(Attributes commandSet, Attributes dataSet) {
            super(commandSet, dataSet);
        }
    	
    	public String getAffectedSopClassUid() {
    		return getCommandSetAttribute(Tag.AffectedSOPClassUID, String.class);
    	}
    	
    	public String getAffectedSopInstanceUid() {
    		return getCommandSetAttribute(Tag.AffectedSOPInstanceUID, String.class);
    	}
    	
    	@Override
        public String toString() {
    		return "StoreScuResponse: [\n"
    				+ "messageIdBeingRespondedTo=" + getMessageIdBeingRespondedTo() + ",\n"
                    + "status=" + getStatusCode() + ",\n"
                    + "affectedSopClassUid=" + getAffectedSopClassUid() + ",\n"
                    + "affectedSopInstanceUid=" + getAffectedSopInstanceUid() + "]";
        }
    	
	}
	
}