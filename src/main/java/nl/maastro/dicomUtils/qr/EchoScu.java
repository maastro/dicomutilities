/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is part of dcm4che, an implementation of DICOM(TM) in
 * Java(TM), hosted at https://github.com/dcm4che.
 *
 * The Initial Developer of the Original Code is
 * Agfa Healthcare.
 * Portions created by the Initial Developer are Copyright (C) 2017
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s): -
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */

package nl.maastro.dicomUtils.qr;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.dcm4che3.data.UID;
import org.dcm4che3.net.ApplicationEntity;
import org.dcm4che3.net.Association;
import org.dcm4che3.net.Connection;
import org.dcm4che3.net.Device;
import org.dcm4che3.net.DimseRSP;
import org.dcm4che3.net.IncompatibleConnectionException;
import org.dcm4che3.net.pdu.AAssociateRQ;
import org.dcm4che3.net.pdu.PresentationContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility class to check if a DiCOM connection can be established
 */
public class EchoScu {
    
    private static final Logger logger = LoggerFactory.getLogger(EchoScu.class);

	/**
	 * Tests for a valid connection using given parameters with default local IP port
	 * @param localAeTitle : the local AETitle
	 * @param remoteAeTitle : the remote AETitle 
	 * @param remoteHostName : the remote host name
	 * @param remotePort : the remote IP port
	 * @return {@literal true} if the connection was successful, otherwise {@literal false}
	 */
	public static boolean execute(String localAeTitle,  String remoteAeTitle, String remoteHostName, 
	        int remotePort) {
		return execute(localAeTitle, -1, remoteAeTitle, remoteHostName, remotePort);
	}

	/**
	 * Tests for a valid connection using given parameters
	 * @param localAeTitle : the local AETitle
	 * @param localPort : the local IP port
	 * @param remoteAeTitle : the remote AETitle 
	 * @param remoteHostName : the remote host name
	 * @param remotePort : the remote IP port
	 * @return {@literal true} if the connection was successful, otherwise {@literal false}
	 */
	public static boolean execute(String localAeTitle, int localPort, 
	        String remoteAeTitle, String remoteHostName, int remotePort) {
	    
		AAssociateRQ request = new AAssociateRQ();

		Device localDevice = new Device("echoscu");
		Connection localConnection = new Connection();
		localDevice.addConnection(localConnection);
		ApplicationEntity localAe = new ApplicationEntity(localAeTitle);
		localDevice.addApplicationEntity(localAe);
		localAe.addConnection(localConnection);

		request.addPresentationContext(
				new PresentationContext(1, UID.Verification,
						UID.ImplicitVRLittleEndian));

		Connection remoteConnection = new Connection();
		request.setCalledAET(remoteAeTitle);
		remoteConnection.setHostname(remoteHostName);
		remoteConnection.setPort(remotePort);//remote port

		if (localPort > 0)
		{
			localConnection.setPort(localPort);
		}

		int maxOpsInvoked = 0;
		localConnection.setMaxOpsInvoked(maxOpsInvoked);
		int maxOpsPerformed = 0;
		localConnection.setMaxOpsPerformed(maxOpsPerformed);
		localConnection.setPackPDV(true);
		localConnection.setTcpNoDelay(true);

		remoteConnection.setTlsProtocols(localConnection.getTlsProtocols());
		remoteConnection.setTlsCipherSuites(localConnection.getTlsCipherSuites());

		ExecutorService executorService = Executors.newSingleThreadExecutor();
		ScheduledExecutorService scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
		localDevice.setExecutor(executorService);
		localDevice.setScheduledExecutor(scheduledExecutorService);
		
		boolean result;
		Association as = null;
		try {
			as = localAe.connect(remoteConnection, request);
			DimseRSP echo = as.cecho();
			while(echo.next());
			result = true;
		} catch (IOException | InterruptedException | GeneralSecurityException | IncompatibleConnectionException ex ) {
		    logger.error("Error while performing C-ECHO", ex);
			result = false;
		} finally {
			if (as != null) {
				try {				
					if (as.isReadyForDataTransfer())
						as.release();
					as.waitForSocketClose();
				} catch (Exception ex) {
				    logger.error("Could not close the association", ex);
				}
			}
			executorService.shutdown();
			scheduledExecutorService.shutdown();
		}
		return result;
	}
}
