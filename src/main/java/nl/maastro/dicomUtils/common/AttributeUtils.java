package nl.maastro.dicomUtils.common;

import java.util.Date;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Sequence;

public class AttributeUtils {
	
	private static final int UNKNOWN = -1;
	
	public static <T> T getAttributeValue(Attributes attributesSet, int tag, Class<T> dataType) throws UnsupportedOperationException {
		if (dataType.equals(String.class)) {
            return dataType.cast(attributesSet.getString(tag));
		} else if (dataType.equals(Integer.class)) {
            return dataType.cast(getInteger(attributesSet, tag));
        } else if (dataType.equals(Date.class)) {
            return dataType.cast(attributesSet.getDate(tag));
        } else if (dataType.equals(Sequence.class)) {
            return dataType.cast(attributesSet.getSequence(tag));
        } else if (dataType.equals(Double.class)) {
            return dataType.cast(getDouble(attributesSet, tag));
            } else if (dataType.equals(Float.class)) {
            return dataType.cast(getFloat(attributesSet, tag));
        } else {
            throw new UnsupportedOperationException("Not implemented for dataType=" + dataType);
        }
    }
	
	public static Double getDouble(Attributes attributesSet, int tag) {
		if (attributesSet.containsValue(tag)) {
			return attributesSet.getDouble(tag, UNKNOWN);
		} else {
			return null;
		}
	}
	
	public static Float getFloat(Attributes attributesSet, int tag) {
		if (attributesSet.containsValue(tag)) {
			return attributesSet.getFloat(tag, UNKNOWN);
		} else {
			return null;
		}
	}
	
	public static Integer getInteger(Attributes attributesSet, int tag) {
		if (attributesSet.containsValue(tag)) {
			return attributesSet.getInt(tag, UNKNOWN);
		} else {
			return null;
		}
	}
	
}
