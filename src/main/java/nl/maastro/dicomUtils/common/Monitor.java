package nl.maastro.dicomUtils.common;

import java.net.Socket;

import org.dcm4che3.net.Connection;
import org.dcm4che3.net.ConnectionMonitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Monitor implements ConnectionMonitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(Monitor.class);

    @Override
    public void onConnectionEstablished(Connection localConnection, Connection remoteConnection, 
            Socket s) {
        LOGGER.trace("connection established");
        LOGGER.trace("local host: " + s.getLocalAddress());
        LOGGER.trace("local port: " + s.getLocalPort());
        LOGGER.trace("remote address: " + s.getInetAddress());
        LOGGER.trace("remote port: " + s.getPort());
    }

    @Override
    public void onConnectionFailed(Connection localConnection, Connection remoteConnection, 
            Socket s, Throwable e) {
        LOGGER.warn("connection failed", e);
        LOGGER.warn("local host: " + s.getLocalAddress());
        LOGGER.warn("local port: " + s.getLocalPort());
        LOGGER.warn("remote address: " + s.getInetAddress());
        LOGGER.warn("remote port: " + s.getPort());
    }

    @Override
    public void onConnectionRejectedBlacklisted(Connection localConnection, Socket s) {
        LOGGER.warn("connection blacklisted");
        LOGGER.warn("local host: " + s.getLocalAddress());
        LOGGER.warn("local port: " + s.getLocalPort());
        LOGGER.warn("remote address: " + s.getInetAddress());
        LOGGER.warn("remote port: " + s.getPort());
    }

    @Override
    public void onConnectionRejected(Connection localConnection, Socket s, Throwable e) {
        LOGGER.warn("connection rejected", e);
        LOGGER.warn("local host: " + s.getLocalAddress());
        LOGGER.warn("local port: " + s.getLocalPort());
        LOGGER.warn("remote address: " + s.getInetAddress());
        LOGGER.warn("remote port: " + s.getPort());
    }

    @Override
    public void onConnectionAccepted(Connection localConnection, Socket s) {
        LOGGER.trace("connection accepted");
        LOGGER.trace("local host: " + s.getLocalAddress());
        LOGGER.trace("local port: " + s.getLocalPort());
        LOGGER.trace("remote address: " + s.getInetAddress());
        LOGGER.trace("remote port: " + s.getPort());
    }

}
