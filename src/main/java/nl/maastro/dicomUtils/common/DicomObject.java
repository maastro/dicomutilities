package nl.maastro.dicomUtils.common;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.io.DicomInputStream;
import org.dcm4che3.io.DicomOutputStream;

/**
 * DicomObject represents one DICOM generic DICOM file through which all DICOM tags can be obtained.
 */
public class DicomObject {

    private final File file;
	private final Attributes fileMetaInformation;
	
	private Attributes dataSet = null;
	

	/**
	 * Constructs a DICOM object using the input file to retrieve the file meta information 
	 * and data set.
	 * @param pathname Pathname of DICOM file.
	 * @throws IOException if input stream could not be created from file.
	 */
	public DicomObject(String pathname) throws IOException {
		this(new File(pathname));
	}
	
	/**
	 * Constructs a DICOM object using the input file to retrieve the file meta information 
	 * and data set.
	 * @param path Path to DICOM file.
	 * @throws IOException if input stream could not be created from file.
	 */
	public DicomObject(Path path) throws IOException {
		this(path.toFile());
	}
	
	/**
	 * Constructs a DICOM object using the input file to retrieve the file meta information 
	 * and data set.
	 * @param file Dicom file
	 * @throws IOException if input stream could not be created from file.
	 */
	public DicomObject(File file) throws IOException {
		this.file = file;
		try (DicomInputStream inputStream = new DicomInputStream(file)) {
			this.fileMetaInformation = inputStream.readFileMetaInformation();
		}
	}

	/**
	 * Returns the file of this DICOM object.
	 * @return File associated with this DICOM object.
	 */
	public File getFile() {
		return this.file;
	}
	
	/**
	 * Returns the file meta information of the DICOM object.
	 * @return File meta information.
	 */
	public Attributes getFileMetaInformation() {
		return this.fileMetaInformation;
	}
	
	/**
	 * Lazy-loads and returns the file data set.
	 * 
	 * @return Array of DICOM attributes comprising of the data set
	 * @throws IOException 
	 */
	public Attributes getDataSet() throws IOException {
		if (this.dataSet == null) {
			try (DicomInputStream inputStream = new DicomInputStream(this.file)) {
				this.dataSet = inputStream.readDataset();
			}
		}
		return this.dataSet;
	}
	
	public <T> T getAttribute(int tag, Class<T> dataType) throws IOException {
		return AttributeUtils.getAttributeValue(this.getDataSet(), tag, dataType);
	}
	
	/**
	 * Retrieves the modality from the modality tag in the DICOM file.
	 * @return modality of this DICOM object.
	 * @throws IOException 
	 */
	public String getModality() throws IOException {
		return getAttribute(Tag.Modality, String.class);
	}

	/**
	 * Writes out this object to a specified file
	 * 
	 * @param outputFile Ouput file
	 * @throws IOException 
	 */
	public void writeToFile(File outputFile) throws IOException {
		outputFile.getParentFile().mkdirs();
		try (DicomOutputStream outputStream = new DicomOutputStream(outputFile)) {
			outputStream.writeDataset(this.fileMetaInformation, getDataSet());
		}
	}

	@Override
	public String toString() {
		return "DicomObject [file=" + file + ", fileMetaInformation=" + fileMetaInformation + "]";
	}
	
}
