package nl.maastro.dicomUtils.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * General status classes for response statuses of DIMSE services.
 * 
 * @see <a href="http://dicom.nema.org/medical/dicom/current/output/chtml/part07/chapter_C.html">
 * http://dicom.nema.org/medical/dicom/current/output/chtml/part07/chapter_C.html</a>
 * 
 */
public enum DimseStatusClass {
	
	SUCCESS, WARNING, FAILURE, CANCEL, PENDING, UNKNOWN;

	private static final Logger logger = LoggerFactory.getLogger(DimseStatusClass.class);
	
	public static DimseStatusClass fromStatusCode(Integer statusCode) {
		if (statusCode == null) {
			return null;
		} else if (statusCode ==  0x0000) {
			return SUCCESS;
		} else if (statusCode == 0x0001 || statusCode == 0x0107 || statusCode == 0x0116 
				|| (statusCode >= 0xB000 && statusCode <= 0xBFFF)) {
			return WARNING;
		} else if ((statusCode >= 0x0100 && statusCode <= 0x01FF)
				|| (statusCode >= 0x0200 && statusCode <= 0x02FF)
				|| (statusCode >= 0xA000 && statusCode <= 0xAFFF) 
				|| (statusCode >= 0xC000 && statusCode <= 0xCFFF)) {
			return FAILURE;
		} else if (statusCode == 0xFE00) {
			return CANCEL;
		} else if (statusCode == 0xFF00 || statusCode == 0xFF01 ) {
			return PENDING;
		} else {
			logger.warn("Unknown status code: " + statusCode);
			return UNKNOWN;
		}
    }
}
