package nl.maastro.dicomUtils.common;

import org.dcm4che3.data.Attributes;
import org.dcm4che3.data.Tag;
import org.dcm4che3.net.Status;

public class DimseResponse {
    
	public static final String STATUS_SUCCESS = "Operation/notification completed successfully";
	public static final String STATUS_CANCEL = "Operation/notification terminated due to CANCEL indication";
	public static final String STATUS_PENDING = "Operation/Notification is pending";
	public static final String STATUS_SOP_CLASS_NOT_SUPPORTED = "SOP Class not supported";
	public static final String STATUS_NOT_AUTHORIZED = "Not authorized";
	public static final String STATUS_DUPLICATE_INVOCATION = "Duplicate invocation";
	public static final String STATUS_UNRECOGNIZED_OPERATION = "Unrecognized operation";
	public static final String STATUS_MISTYPED_ARGUMENT = "Mistyped argument";
	public static final String STATUS_RESOURCE_LIMITATION = "Resource limitation";
			
    private Attributes commandSet;
    private Attributes dataSet;
    
    public DimseResponse(Attributes commandSet, Attributes dataSet) {
        this.commandSet = commandSet;
        this.dataSet = dataSet;
    }

    public Attributes getCommandSet() {
        return commandSet;
    }

    public Attributes getDataSet() {
        return dataSet;
    }
    
    public Integer getMessageIdBeingRespondedTo() {
    	return getCommandSetAttribute(Tag.MessageIDBeingRespondedTo, Integer.class);
    }
    
    public Integer getStatusCode() {
        return getCommandSetAttribute(Tag.Status, Integer.class);
    }
    
    public boolean hasStatus(DimseStatusClass otherStatusClass) {
    	Integer statusCode = getStatusCode();
    	DimseStatusClass statusClass = DimseStatusClass.fromStatusCode(statusCode);
    	return statusClass == otherStatusClass;
    }
    
    public static String getStatusDescription(Integer statusCode) {
    	if (statusCode == null) {
    		return null;
    	}
    	switch (statusCode) {
    		case Status.Success:
    			return STATUS_SUCCESS;
    		case Status.Cancel:
    			return STATUS_CANCEL;
    		case Status.Pending:
    			return STATUS_PENDING;
    		case Status.SOPclassNotSupported:
    			return STATUS_SOP_CLASS_NOT_SUPPORTED;
    		case Status.NotAuthorized:
    			return STATUS_NOT_AUTHORIZED;
    		case Status.DuplicateInvocation:
    			return STATUS_DUPLICATE_INVOCATION;	
    		case Status.UnrecognizedOperation:
    			return STATUS_UNRECOGNIZED_OPERATION;
    		case Status.MistypedArgument:
    			return STATUS_MISTYPED_ARGUMENT;
    		case Status.ResourceLimitation:
    			return STATUS_RESOURCE_LIMITATION;
    		default:
    			DimseStatusClass statusClass = DimseStatusClass.fromStatusCode(statusCode);
        		return statusClass.toString();
    	}
    }
    
    public<T> T getCommandSetAttribute(int tag, Class<T> dataType) throws UnsupportedOperationException {
        if (this.commandSet == null) {
            return null;
        } else {
            return AttributeUtils.getAttributeValue(this.commandSet, tag, dataType);
        }
    }
    
    public<T> T getDataSetAttribute(int tag, Class<T> dataType) throws UnsupportedOperationException {
        if (this.dataSet == null) {
            return null;
        } else {
            return AttributeUtils.getAttributeValue(this.dataSet, tag, dataType);
        }
    }
    
    @Override
    public String toString() {
        return "DimseResponse: [\n"
        		+ "messageIdBeingRespondedTo=" + getMessageIdBeingRespondedTo() + ",\n"
                + "statusCode=" + getStatusCode() + "]";
    }

}
